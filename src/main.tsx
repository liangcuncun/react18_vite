import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Route, Link, Routes } from "react-router-dom";
import BatchState from "./routes/BatchState";
import Suspense from "./routes/Suspense";
import SuspenseList from "./routes/SuspenseList";
import StartTransition from "./routes/StartTransition";
import UseDeferredValue from "./routes/UseDeferredValue";
import UseTransition from "./routes/UseTransition";
// legacy模式ReactDOM.renders会同步渲染
// createRoot会启用concurrent并发模式
// 旧的legacy模式ReactDOM.renders会同步渲染 渲染是同步的
// ReactDOM.render(
//   <Router>
//     <ul>
//       <li>
//         <Link to="/BatchState">BatchState</Link>
//       </li>
//     </ul>
//     <Routes>
//       <Route path="/BatchState" element={<BatchState />} />
//     </Routes>
//   </Router>,
//   document.getElementById("root")!
// );
// 在新的模式下，setState都是批量更新的，seTimeOut内也是批量的
ReactDOM.createRoot(document.getElementById("root")!).render(
  <Router>
    <ul>
      <li>
        <Link to="/BatchState">BatchState</Link>
      </li>
      <li>
        <Link to="/Suspense">Suspense</Link>
      </li>
      <li>
        <Link to="/SuspenseList">SuspenseList</Link>
      </li>
      <li>
        <Link to="/StartTransition">StartTransition</Link>
      </li>
      <li>
        <Link to="/UseDeferredValue">UseDeferredValue</Link>
      </li>
      <li>
        <Link to="/UseTransition">UseTransition</Link>
      </li>
    </ul>
    <Routes>
      <Route path="/BatchState" element={<BatchState />} />
      <Route path="/Suspense" element={<Suspense />} />
      <Route path="/SuspenseList" element={<SuspenseList />} />
      <Route path="/StartTransition" element={<StartTransition />} />
      <Route path="/UseDeferredValue" element={<UseDeferredValue />} />
      <Route path="/UseTransition" element={<UseTransition />} />
    </Routes>
  </Router>
);
