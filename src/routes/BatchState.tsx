import React, { Component } from "react";
interface Props {}
interface State {
  count: number;
}
class BatchState extends Component<Props, State> {
  state = { count: 0 };
  // 旧模式0023
  // 新模式0011
  handleCLick = () => {
    this.setState({ count: this.state.count + 1 });
    console.log("count", this.state.count);
    this.setState({ count: this.state.count + 1 });
    console.log("count", this.state.count);
    setTimeout(() => {
      this.setState({ count: this.state.count + 1 });
      console.log("count", this.state.count);
      this.setState({ count: this.state.count + 1 });
      console.log("count", this.state.count);
    }, 0);
  };

  render() {
    return (
      <div>
        <p>{this.state.count}</p>
        <button onClick={this.handleCLick}>+</button>
      </div>
    );
  }
}
export default BatchState;
