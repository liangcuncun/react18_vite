import React, { Component, Suspense, SuspenseList } from "react";
import ErrorBoundary from "../components/ErrorBoundary";
function createResource(promise: Promise<any>) {
  let status = "pending";
  let result: any;
  return {
    read() {
      if (status === "success" || status === "error") {
        return result;
      } else {
        throw promise.then(
          (data: any) => {
            status = "success";
            result = data;
          },
          (error: any) => {
            status = "error";
            result = error;
          }
        );
      }
    },
  };
}

function fetchData(id: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ success: true, data: { id, name: "姓名" + id } });
      //reject({success:false,message:'获取数据发生了错误'});
    }, 1000 * id);
  });
}

let userResourceMap: any = {
  1: createResource(fetchData(1)),
  2: createResource(fetchData(2)),
  3: createResource(fetchData(3)),
};
interface UserProps {
  id: number;
}
function User(props: UserProps) {
  const result = userResourceMap[props.id].read();
  if (result.success) {
    let user = result.data;
    return (
      <p>
        {user.id}:{user.name}
      </p>
    );
  } else {
    return <p>{result.message}</p>;
  }
}

export default class extends Component {
  render() {
    return (
      <ErrorBoundary fallback={<h1>出错了</h1>}>
        {/* backwards 从后往前显示;collapsed 仅显示列表中下一个 fallback */}
        <SuspenseList revealOrder="backwards" tail="collapsed">
          <Suspense fallback={<h1>加载用户3......</h1>}>
            <User id={3} />
          </Suspense>
          <Suspense fallback={<h1>加载用户2......</h1>}>
            <User id={2} />
          </Suspense>
          <Suspense fallback={<h1>加载用户1......</h1>}>
            <User id={1} />
          </Suspense>
        </SuspenseList>
      </ErrorBoundary>
    );
  }
}
