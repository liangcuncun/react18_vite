import React, { Component, Suspense } from "react";
import ErrorBoundary from "../components/ErrorBoundary";
function createResource(promise: Promise<any>) {
  let status = "pending";
  let result: any;
  return {
    read() {
      if (status === "success" || status === "error") {
        return result;
      } else {
        throw promise.then(
          (data: any) => {
            status = "success";
            result = data;
          },
          (error: any) => {
            status = "error";
            result = error;
          }
        );
      }
    },
  };
}

function fetchData(id: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ success: true, data: { id, name: "张三" } });
      //reject({success:false,message:'获取数据发生了错误'});
    }, 2000);
  });
}
const initialResource = createResource(fetchData(1));
function User() {
  const result = initialResource.read();
  if (result.success) {
    let user = result.data;
    return (
      <p>
        {user.id}:{user.name}
      </p>
    );
  } else {
    return <p>{result.message}</p>;
  }
}

export default class extends Component {
  render() {
    return (
      <ErrorBoundary fallback={<h1>出错了</h1>}>
        <Suspense fallback={<h1>加载中</h1>}>
          <User />
        </Suspense>
      </ErrorBoundary>
    );
  }
}
